const express = require('express')
const router = express.Router()
const commentController = require('../controllers/commentController.js')
const { verifyRole } = require('../auth/auth')
const { validateComment, validateDeleteComment } = require('../middleware/commentValidator')

/**
 * All API need authorized as customer
 */
router.use(verifyRole('customer'))

router.post('/', validateComment, commentController.createComment)
router.delete('/:id', validateDeleteComment, commentController.deleteCommentById)

module.exports = router
