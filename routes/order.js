const express = require('express')
const router = express.Router()
const orderController = require('../controllers/orderController')
const { verifyRole } = require('../auth/auth')
const { validateNewOrder, validateUpdateOrderStatus } = require('../middleware/orderValidator.js')
const { ORDER_STATUSES } = require('accommerce-helpers')

/**
 * All API need authorized as customer
 */
router.use(verifyRole('customer'))

router.get('/', orderController.searchOrders)
router.post('/', validateNewOrder, orderController.createOrder)
router.put(
    '/:orderId/status/confirm-received',
    validateUpdateOrderStatus(ORDER_STATUSES.DELIVERED),
    orderController.confirmReceived
)
router.put(
    '/:orderId/status/cancel',
    validateUpdateOrderStatus(ORDER_STATUSES.CANCELLED_BY_CUSTOMER),
    orderController.cancelOrder
)

module.exports = router
