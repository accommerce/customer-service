const express = require('express')
const router = express.Router()
const cartController = require('../controllers/cartController.js')
const { verifyRole } = require('../auth/auth')
const {
    validateNewCartItem,
    validateEditCartItem,
    validateDeleteCartItems,
} = require('../middleware/cartValidator.js')

/**
 * All API need authorized as customer
 */
router.use(verifyRole('customer'))

router.get('/', cartController.getCartItems)
router.post('/', validateNewCartItem, cartController.addItemToCart)
router.delete('/', validateDeleteCartItems, cartController.deleteCartItems)
router.put('/:id', validateEditCartItem, cartController.editCartItem)

module.exports = router
