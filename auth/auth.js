const { Users } = require('../models/models')
const secretKey = process.env.SECRET_KEY || 'JFJH7LK9KVIC7G4OR9L75IHSTIV81H0G'

const auth = require('accommerce-authentication')

module.exports = auth(Users, secretKey)
