const { sendResponse, CustomError } = require('accommerce-helpers')
const { createComment, deleteCommentById } = require('../actions/commentActions.js')

exports.createComment = async (req, res, next) => {
    try {
        const comment = await createComment(req.body)

        sendResponse(comment)(req, res, next)
    } catch (error) {
        next(error)
    }
}

exports.deleteCommentById = async (req, res, next) => {
    try {
        const { id: commentId } = req.params

        await deleteCommentById(commentId)

        sendResponse(true, `Xóa comment thành công`, `Deleted comment successfully`)(req, res, next)
    } catch (error) {
        next(error)
    }
}
