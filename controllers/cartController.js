const { sendResponse } = require('accommerce-helpers')
const {
    addItemToCart,
    getCartItems,
    editCartItem,
    deleteCartItems,
} = require('../actions/cartActions.js')

exports.addItemToCart = async (req, res, next) => {
    try {
        const args = req.body

        const cart = await addItemToCart(args)

        sendResponse(
            cart,
            `Thêm sản phẩm vào giỏ thành công`,
            'Add to cart successfully!',
            201
        )(req, res, next)
    } catch (error) {
        next(error)
    }
}

exports.getCartItems = async (req, res, next) => {
    try {
        const cart = await getCartItems(req.user._id)

        sendResponse(cart)(req, res, next)
    } catch (error) {
        next(error)
    }
}

exports.editCartItem = async (req, res, next) => {
    try {
        const { id, quantity, size } = { ...req.params, ...req.body }
        const cart = await editCartItem({ id, quantity, size })

        sendResponse(cart)(req, res, next)
    } catch (error) {
        next(error)
    }
}

exports.deleteCartItems = async (req, res, next) => {
    try {
        const { cartItemIds } = req.body

        const { deletedCount } = await deleteCartItems(cartItemIds)

        sendResponse(
            true,
            `Đã xóa ${deletedCount} sản phẩm khỏi giỏ`,
            `Removed ${deletedCount} item(s) from cart`
        )(req, res, next)
    } catch (error) {
        next(error)
    }
}
