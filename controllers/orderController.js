const { sendResponse } = require('accommerce-helpers')
const {
    createOrder,
    searchOrders,
    cancelOrder,
    confirmReceived,
} = require('../actions/orderActions')
const { ORDER_STATUSES } = require('accommerce-helpers')

exports.createOrder = async (req, res, next) => {
    try {
        const args = req.body

        const order = await createOrder(args)

        sendResponse(order, `Đặt hàng thành công`, `Ordered successfully`, 201)(req, res, next)
    } catch (error) {
        next(error)
    }
}

exports.searchOrders = async (req, res, next) => {
    try {
        const { status } = req.query
        const customer = req.user._id

        const orders = await searchOrders({ status, customer })

        sendResponse(orders)(req, res, next)
    } catch (error) {
        next(error)
    }
}

exports.confirmReceived = async (req, res, next) => {
    try {
        const { orderId } = req.params

        const updatedOrder = await confirmReceived(orderId)

        sendResponse(updatedOrder, `Thay đổi trạng thái đơn hàng thành công`, 200)(req, res, next)
    } catch (error) {
        next(error)
    }
}

exports.cancelOrder = async (req, res, next) => {
    try {
        const { orderId } = req.params

        const updatedOrder = await cancelOrder(orderId)

        sendResponse(updatedOrder, `Thay đổi trạng thái đơn hàng thành công`, 200)(req, res, next)
    } catch (error) {
        next(error)
    }
}
