const Bluebird = require('bluebird')
const {
    Orders,
    OrderItems,
    CartItems,
    Shops,
    Products,
    Sizes,
    Connection,
} = require('../models/models')
const { ORDER_STATUSES, CustomError } = require('accommerce-helpers')

exports.createOrder = async (args) => {
    const session = await Connection.startSession()

    try {
        session.startTransaction()
        const { cartItems, orderItems, ...rest } = args

        const orders = await Orders.create([rest], { session })
        const order = orders[0]

        const orderItemsWithOrderId = orderItems.map((item) => ({ ...item, order: order._id }))
        const createdOrderItems = await OrderItems.create(orderItemsWithOrderId, { session })

        await Bluebird.map(
            cartItems,
            async (itemId) => {
                const { product, size, quantity } = await CartItems.findById(itemId)
                    .lean()
                    .session(session)

                await Sizes.updateOne(
                    { product, name: size },
                    { $inc: { numberInStock: -quantity } },
                    { session }
                )
            },
            { concurrency: cartItems.length }
        )

        await CartItems.deleteMany({ _id: { $in: cartItems } }).session(session)

        const createdOrder = await Orders.findById(order._id).lean().session(session)

        await session.commitTransaction()

        return { ...createdOrder, items: createdOrderItems }
    } catch (error) {
        session.abortTransaction()
        throw error
    } finally {
        session.endSession()
    }
}

exports.searchOrders = async (args) => {
    const { status, customer } = args

    const query = { customer }

    if (status) {
        query.status = status
    }

    const orders = await Orders.find(query)
        .populate({
            path: 'shop',
            model: Shops,
        })
        .sort('-_id')
        .lean()

    return Bluebird.map(
        orders,
        async (order) => {
            const orderItems = await OrderItems.find({ order: order._id })
                .populate({
                    path: 'product',
                    model: Products,
                })
                .lean()

            const totalPrice = orderItems.reduce(
                (total, item) => total + item.quantity * item.price,
                0
            )

            return { ...order, totalPrice, items: orderItems }
        },
        { concurrency: orders.length }
    )
}

const _updateSizesWhenCancelOrder = async (orderId, session) => {
    const orderItems = await OrderItems.find({ order: orderId }).lean()

    return Bluebird.map(
        orderItems,
        async (item) => {
            const { product, size, quantity } = item

            await Sizes.updateOne(
                { product, name: size },
                { $inc: { numberInStock: quantity } },
                { session }
            )
        },
        { concurrency: orderItems.length }
    )
}

exports.cancelOrder = async (orderId) => {
    const session = await Connection.startSession()
    try {
        session.startTransaction()

        const [cancelledOrder] = await Bluebird.all([
            Orders.findByIdAndUpdate(
                orderId,
                { $set: { status: ORDER_STATUSES.CANCELLED_BY_CUSTOMER } },
                { new: true, session }
            ).lean(),
            _updateSizesWhenCancelOrder(orderId, session),
        ])

        await session.commitTransaction()

        return cancelledOrder
    } catch (error) {
        await session.abortTransaction()
        throw error
    } finally {
        await session.endSession()
    }
}

exports.confirmReceived = async (orderId) => {
    const session = await Connection.startSession()
    try {
        session.startTransaction()

        const order = await Orders.findByIdAndUpdate(
            orderId,
            { $set: { status: ORDER_STATUSES.DELIVERED } },
            { new: true, session }
        ).lean()

        const orderItems = await OrderItems.find({ order: orderId }).session(session).lean()
        await Bluebird.map(
            orderItems,
            async (item) => {
                const { product, quantity } = item

                await Products.updateOne(
                    { _id: product },
                    { $inc: { sold: quantity } },
                    { session }
                )
            },
            { concurrency: orderItems.length }
        )

        await session.commitTransaction()

        return order
    } catch (error) {
        await session.abortTransaction()
        throw error
    } finally {
        session.endSession()
    }
}
