const e = require('express')
const { Comments, Products, Connection } = require('../models/models')

const _recalculateProductRating = async (productId, session) => {
    const allComments = await Comments.find({ product: productId })
        .select('rating')
        .session(session)
        .lean()

    const totalRating = allComments.reduce((total, comment) => total + comment.rating, 0)
    const newProductRating = allComments.length ? totalRating / allComments.length : 5

    await Products.updateOne(
        { _id: productId },
        { $set: { rating: Math.round(newProductRating * 100) / 100 } },
        { session }
    )
}

exports.createComment = async ({ author, product, comment, rating, images }) => {
    const session = await Connection.startSession()
    try {
        session.startTransaction()

        const createdComment = await Comments.findOneAndUpdate(
            { author, product },
            { $set: { author, product, comment, rating, images } },
            { upsert: true, new: true, session }
        ).lean()

        await _recalculateProductRating(product, session)

        await session.commitTransaction()

        return createdComment
    } catch (error) {
        await session.abortTransaction()
        throw error
    } finally {
        session.endSession()
    }
}

exports.deleteCommentById = async (commentId) => {
    const session = await Connection.startSession()
    try {
        session.startTransaction()

        const deletedComment = await Comments.findByIdAndDelete(commentId).session(session).lean()

        await _recalculateProductRating(deletedComment.product, session)

        await session.commitTransaction()

        return true
    } catch (error) {
        await session.abortTransaction()
        throw error
    } finally {
        session.endSession()
    }
}
