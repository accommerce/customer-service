const { CartItems, Products, Sizes, Shops, Connection } = require('../models/models')
const { CustomError } = require('accommerce-helpers')

exports.addItemToCart = async ({ product, size, customer, shop, quantity }) => {
    const existCartItem = await CartItems.findOne({ product, size, customer, shop })

    if (!existCartItem) {
        return CartItems.create({ product, size, customer, shop, quantity })
    }

    const newQuantity = quantity + existCartItem.quantity
    const fullSize = await Sizes.findOne({ product, name: size }).lean()

    if (fullSize.numberInStock < newQuantity) {
        throw CustomError(
            `Không còn đủ sản phẩm trong kho hàng. Giỏ hàng đã chứa ${existCartItem.quantity} sản phẩm`,
            `There are not enough products left in stock. Cart has already had ${existCartItem.quantity} products`,
            400
        )
    }

    existCartItem.quantity = newQuantity

    return existCartItem.save()
}

exports.getCartItems = async (customerId) => {
    const cartItems = await CartItems.find({ customer: customerId })
        .populate({
            path: 'product',
            model: Products,
        })
        .populate({
            path: 'shop',
            model: Shops,
        })
        .lean()

    const shopsDictionary = cartItems
        .filter(
            (item, index, self) =>
                self.findIndex(
                    (vItem) => vItem.shop._id.toString() === item.shop._id.toString()
                ) === index
        ) // Unique by shop _id
        .reduce((acc, item) => {
            const {
                shop: { _id: shopId, ...shopRest },
            } = item

            return { [shopId]: { ...shopRest, items: [] }, ...acc }
        }, {})

    const uniqueProductIds = cartItems
        .filter(
            (item, index, self) =>
                self.findIndex(
                    (vItem) => vItem.product._id.toString() === item.product._id.toString()
                ) === index
        )
        .map((item) => item.product._id)

    const allSizes = await Sizes.find({ product: { $in: uniqueProductIds } })
        .select('product name numberInStock')
        .lean()

    cartItems.forEach((item) => {
        item.product.sizes = allSizes.filter(
            (size) => size.product.toString() === item.product._id.toString()
        )

        const { shop, customer, ...rest } = item

        shopsDictionary[item.shop._id].items.push(rest)
    })

    const cartItemsGroupByShop = Object.keys(shopsDictionary).map((key) => ({
        _id: key,
        ...shopsDictionary[key],
    }))

    return cartItemsGroupByShop
}

exports.editCartItem = async ({ id, quantity, size }) => {
    const session = await Connection.startSession()
    try {
        session.startTransaction()

        const existItem = await CartItems.findById(id).lean()

        const { size: existSize, product, customer } = existItem

        if (size !== existSize) {
            const { quantity: duplicateQuantity } =
                (await CartItems.findOneAndDelete(
                    { customer, product, size, _id: { $ne: id } },
                    { session }
                ).lean()) || {}

            duplicateQuantity && (quantity += duplicateQuantity)
        }

        const updatedCartItem = await CartItems.findByIdAndUpdate(
            id,
            { $set: { size, quantity } },
            { new: true, session }
        ).lean()

        await session.commitTransaction()

        return updatedCartItem
    } catch (error) {
        await session.abortTransaction()
        throw error
    } finally {
        session.endSession()
    }
}

exports.deleteCartItems = async (ids) => {
    return CartItems.deleteMany({ _id: { $in: ids } })
}
