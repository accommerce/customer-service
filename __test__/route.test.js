const request = require('supertest')
const app = require('../app')

describe('check-page', () => {
  beforeAll(async () => {
  })

  afterAll(async () => {
  })

  describe('GET /', function() {
    it('responds 200', function(done) {
      request(app)
        .get('/')
        .expect(200, done);
    });
  });
});  
