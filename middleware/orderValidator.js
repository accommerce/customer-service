const Bluebird = require('bluebird')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)
const { Sizes, Products, Shops, CartItems, Orders, Users } = require('../models/models')
const { ORDER_STATUSES, ORDER_STATUSES_MAPPING, CustomError } = require('accommerce-helpers')

const newOrderSchema = Joi.object({
    cartItems: Joi.array().items(Joi.objectId()).min(1).required(),
    receivingAddress: Joi.string().required(),
    discount: Joi.objectId(),
})

exports.validateNewOrder = async (req, res, next) => {
    try {
        const { value, error } = newOrderSchema.validate(req.body)

        if (error) {
            error.status = 400
            return next(error)
        }

        const { cartItems } = value
        const customer = req.user._id

        const validatedOrderItems = await Bluebird.map(
            cartItems,
            async (cartItemId) => {
                const item = await CartItems.findOne({ _id: cartItemId, customer })
                    .populate({
                        path: 'product',
                        model: Products,
                        populate: {
                            path: 'shop',
                            model: Shops,
                            populate: {
                                path: 'seller',
                                model: Users,
                            },
                        },
                    })
                    .lean()

                if (!item) {
                    throw CustomError(
                        `Có lỗi xảy ra, vui lòng thử lại sau`,
                        `Error. Please try again later`,
                        400
                    )
                }

                const {
                    product: { _id: productId, name: productName, price, shop, deletedAt },
                    size: sizeName,
                    quantity,
                } = item

                if (!shop.isActive || !shop?.seller?.isActive) {
                    throw CustomError(
                        `Cửa hàng đang tạm dừng hoạt động`,
                        `Shop is temporarily closed`,
                        400
                    )
                }

                if (deletedAt) {
                    throw CustomError(
                        `Sản phẩm ${productName} đã bị xóa bởi người bán`,
                        `Product ${productName} has been sold out or deleted by seller`,
                        400
                    )
                }

                const size = await Sizes.findOne({ product: productId, name: sizeName }).lean()

                if (!size) {
                    throw CustomError(
                        `Không tìm thấy size ${sizeName} của sản phẩm ${productName}`,
                        `Cannot find size ${sizeName} of product ${productName}`,
                        400
                    )
                }

                const { numberInStock } = size
                if (numberInStock < quantity) {
                    throw CustomError(
                        `Sản phẩm ${productName} với size ${sizeName} chỉ còn lại ${numberInStock} sản phẩm trong kho`,
                        `Product ${productName} with size ${sizeName} only ${numberInStock} product(s) left in stock!`,
                        400
                    )
                }

                return {
                    product: productId,
                    size: sizeName,
                    quantity,
                    price,
                    shop: shop._id,
                }
            },
            { concurrency: cartItems.length }
        )

        const shops = [...new Set(validatedOrderItems.map((item) => item.shop.toString()))]
        if (shops.length > 1) {
            throw CustomError(
                `Tất cả sản phẩm phải được bán bởi 1 cửa hàng`,
                'All order items must be sold by one shop!',
                400
            )
        }

        value.orderItems = validatedOrderItems

        value.shop = shops[0]

        const { address: sellingAddress } = await Shops.findById(value.shop).lean()
        value.sellingAddress = sellingAddress

        value.customer = customer

        value.shippingCost = validatedOrderItems.reduce(
            (sum, item) => sum + item.quantity * 1000,
            0
        )

        console.log(value)
        req.body = value
        next()
    } catch (error) {
        next(error)
    }
}

exports.validateUpdateOrderStatus = (status) => async (req, res, next) => {
    try {
        const { orderId } = req.params
        const { error } = Joi.objectId().validate(orderId)

        if (error) {
            error.status = 400
            return next(error)
        }

        const customerId = req.user._id

        const order = await Orders.findOne({ _id: orderId, customer: customerId }).lean()
        if (!order) {
            throw CustomError(`Không tìm thấy đơn hàng`, `Order not found!`, 404)
        }

        if (status === ORDER_STATUSES.CANCELLED_BY_CUSTOMER) {
            if (order.status !== ORDER_STATUSES.WAITING_FOR_SELLER_CONFIRM) {
                throw CustomError(
                    `Bạn chỉ có thể hủy đơn hàng đang trong trạng thái ${
                        ORDER_STATUSES_MAPPING[ORDER_STATUSES.WAITING_FOR_SELLER_CONFIRM]
                    }. Trạng thái hiện tại: ${ORDER_STATUSES_MAPPING[order.status]}`,
                    `Can only cancel order has status ${ORDER_STATUSES.WAITING_FOR_SELLER_CONFIRM}.Current status: ${order.status}`,
                    400
                )
            }
        } else if (status === ORDER_STATUSES.DELIVERED) {
            if (order.status !== ORDER_STATUSES.IN_TRANSIT) {
                throw CustomError(
                    `Bạn chỉ có thể xác nhận đã nhận hàng với đơn hàng có trạng thái ${
                        ORDER_STATUSES_MAPPING[ORDER_STATUSES.IN_TRANSIT]
                    }. Trạng thái hiện tại: ${ORDER_STATUSES_MAPPING[order.status]}`,
                    `Can only confirm received for order has status ${ORDER_STATUSES.IN_TRANSIT}. Current status: ${order.status}`,
                    400
                )
            }
        } else {
            throw CustomError(`Có lỗi xảy ra, vui lòng thử lại sau`, `Error, try again later`, 500)
        }

        next()
    } catch (error) {
        next(error)
    }
}
