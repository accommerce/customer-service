const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)
const { CustomError, ORDER_STATUSES } = require('accommerce-helpers')
const { Orders, OrderItems, Users, Products, Comments } = require('../models/models')

const commentSchema = Joi.object({
    product: Joi.objectId().required(),
    comment: Joi.string().trim().max(1000).required(),
    rating: Joi.number().valid(1, 2, 3, 4, 5, '1', '2', '3', '4', '5').required(),
    images: Joi.array().items(Joi.string().trim()).max(5).default([]),
})

exports.validateComment = async (req, res, next) => {
    try {
        const { value, error } = commentSchema.validate(req.body)
        if (error) {
            throw error
        }

        const { product } = value
        const userId = req.user._id

        const receivedOrders = await Orders.find({
            customer: userId,
            status: ORDER_STATUSES.DELIVERED,
        })
            .select('_id')
            .lean()
        if (!receivedOrders.length) {
            throw CustomError(
                `Bạn chỉ có thể đánh giá sản phẩm đã mua`,
                `You can only rate products you've purchased`,
                400
            )
        }

        const orderItems = await OrderItems.find({
            product,
            order: { $in: receivedOrders.map((order) => order._id) },
        })
            .select('_id')
            .lean()
        if (!orderItems.length) {
            throw CustomError(
                `Bạn chỉ có thể đánh giá sản phẩm đã mua`,
                `You can only rate products you've purchased`,
                400
            )
        }

        value.author = userId
        req.body = value

        next()
    } catch (error) {
        next(error)
    }
}

exports.validateDeleteComment = async (req, res, next) => {
    try {
        const { id: commentId } = req.params

        const { error } = Joi.objectId().validate(commentId)

        if (error) {
            throw error
        }

        const comment = await Comments.findById(commentId).lean()
        if (!comment) {
            throw CustomError(`Đánh giá không tồn tại`, `Comment not found`, 404)
        }

        const userId = req.user._id.toString()
        const { author } = comment

        if (author.toString() !== userId) {
            throw CustomError(
                `Bạn chỉ có thể xóa đánh giá của chính mình`,
                `You can only delete your own comment`,
                403
            )
        }

        next()
    } catch (error) {
        next(error)
    }
}
