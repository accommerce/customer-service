const { CustomError } = require('accommerce-helpers')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)
const { CartItems, Products, Sizes, Shops, Users } = require('../models/models')

const newCartItemSchema = Joi.object({
    product: Joi.objectId().required(),
    size: Joi.string().required(),
    quantity: Joi.number().min(1).max(9999).required(),
})

exports.validateNewCartItem = async (req, res, next) => {
    try {
        const { value, error } = newCartItemSchema.validate(req.body)

        if (error) {
            error.status = 400
            return next(error)
        }

        const existProduct = await Products.findById(value.product)
            .populate({
                path: 'shop',
                model: Shops,
                populate: {
                    path: 'seller',
                    model: Users,
                },
            })
            .lean()

        if (!existProduct) {
            throw CustomError(`Sản phẩm thêm vào giỏ không tồn tại`, `Product not exist`, 400)
        }

        const existSize = await Sizes.findOne({ name: value.size, product: value.product })
            .populate({ path: 'product', model: Products })
            .lean()

        if (!existSize) {
            throw CustomError(
                `Không tìm thấy size ${value.size} của sản phẩm ${existProduct.name}`,
                `Cannot find size ${value.size} of product ${existProduct.name}`,
                400
            )
        }

        if (existSize.numberInStock < value.quantity) {
            throw CustomError(
                `Sản phẩm ${existProduct.name} chỉ còn ${existSize.numberInStock} sản phẩm trong kho`,
                `Product ${existProduct.name} with size ${value.size} only ${existSize.numberInStock} left!`,
                400
            )
        }

        const {
            shop: {
                _id: shopId,
                name: shopName,
                isActive: shopActive,
                seller: { _id: sellerId, isActive: sellerActive },
            },
        } = existProduct

        if (!shopActive || !sellerActive) {
            throw CustomError(
                `Cửa hàng tạm ngừng hoạt động`,
                `Shop ${shopName} temporarily closed!`,
                400
            )
        }

        if (sellerId.toString() === req.user._id.toString()) {
            throw CustomError(
                `Bạn không thể tự mua sản phẩm của mình`,
                `You can't buy your own products!`,
                403
            )
        }

        req.body = { ...value, shop: shopId, customer: req.user._id }
        next()
    } catch (error) {
        next(error)
    }
}

const editCartItemSchema = Joi.object({
    size: Joi.string(),
    quantity: Joi.number().min(1).max(9999),
}).or('size', 'quantity')

exports.validateEditCartItem = async (req, res, next) => {
    try {
        const { id } = req.params
        const userId = req.user._id.toString()
        const { error: idError } = Joi.objectId().validate(id)

        if (idError) {
            idError.status = 400
            return next(idError)
        }

        const { value, error } = editCartItemSchema.validate(req.body)

        if (error) {
            error.status = 400
            return next(error)
        }

        const cartItem = await CartItems.findById(id)
            .populate({
                path: 'product',
                model: Products,
            })
            .lean()

        if (!cartItem) {
            throw CustomError(`Không tìm thấy item`, `Cart item ${id} not found!`, 404)
        }

        if (cartItem.customer.toString() !== userId) {
            throw CustomError(
                `Có lỗi xảy ra. Vui lòng thử lại sau`,
                `Cart item ${id} is not your!`,
                403
            )
        }

        const { product: existProduct, size: existSize, quantity: existQuantity } = cartItem
        const sizes = await Sizes.find({ product: existProduct._id }).lean()
        const sizeArray = sizes.map((size) => size.name)
        let { size, quantity } = value

        if (size) {
            if (!sizeArray.includes(size)) {
                throw CustomError(
                    `Sản phẩm ${existProduct.name} chỉ có các sizes: ${sizeArray}`,
                    `Product ${existProduct.name} only has sizes: ${sizeArray}. You have selected: '${size}'!`,
                    400
                )
            }
        } else {
            size = existSize
        }

        const { numberInStock } = sizes.find((vSize) => vSize.name === size)

        if (quantity) {
            if (!numberInStock) {
                throw CustomError(
                    `Sản phẩm ${existProduct.name} đã bán hết`,
                    `Product ${existProduct.name} has sold out!`,
                    400
                )
            } else if (numberInStock < quantity) {
                throw CustomError(
                    `Chỉ còn ${numberInStock} sản phẩm trong kho`,
                    `Only ${numberInStock} product(s) left in stock!`,
                    400
                )
            }
        } else {
            quantity = existQuantity
        }

        req.body = { size, quantity }
        next()
    } catch (error) {
        next(error)
    }
}

const deleteCartItemsSchema = Joi.object({
    cartItemIds: Joi.array().items(Joi.objectId()).min(1).required(),
})

exports.validateDeleteCartItems = async (req, res, next) => {
    try {
        const { value, error } = deleteCartItemsSchema.validate(req.body)

        if (error) {
            error.status = 400
            return next(error)
        }

        const userId = req.user._id.toString()
        const cartItems = await CartItems.find({ _id: { $in: value.cartItemIds } })
            .select('customer')
            .lean()

        cartItems.forEach((item) => {
            if (item.customer.toString() !== userId) {
                throw CustomError(
                    `Có lỗi xảy ra. Vui lòng thử lại sau`,
                    `Cart item ${item._id} is not your!`,
                    403
                )
            }
        })

        next()
    } catch (error) {
        next(error)
    }
}
